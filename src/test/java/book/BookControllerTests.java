/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package book;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringRunner.class)
@SpringBootConfiguration
@ContextConfiguration
@SpringBootTest
public class BookControllerTests {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = mockMvc = standaloneSetup(new BookController())
                .build();
    }

    @Test
    public void validateGetBookByIdWhenValidId() throws Exception {

        int id= 1;
        mockMvc
                .perform(get("/api/book/" + id))
                .andExpect(status().isOk())
                .andExpect(
                        content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE ))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.title").value("De Grote Bosatlas"));

    }

//    @Test
//    public void validateGetBookByIdWithInvalidId() throws Exception {
//
//        int id= 999;
//        mockMvc
//                .perform(get("/api/book/" + id))
//                .andExpect(status().isNotFound())
//                // .andExpect(jsonPath("$.error").value("A book with id " + id + " was not found!"))
//        ;
//
//    }


}
