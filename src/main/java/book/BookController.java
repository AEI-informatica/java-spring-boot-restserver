package book;

import category.Category;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
// Hier geef je de mapping die geldt voor alle endpoints in deze controller.
// Wordt voor aan URL toegevoegd.
@RequestMapping("/api")
public class BookController {

    private final AtomicInteger counter = new AtomicInteger();
    private static final Logger log = LoggerFactory.getLogger(BookController.class);
    private ArrayList<Book> bookRepository;

    /**
     * Maak een lijst met boeken waarin we kunnen zoeken.
     */
    public BookController() {
        Category catProgramming = new Category("Programming", "Books on programming");
        Category catCreative = new Category("Creative", "Books on creativity");
        Category catEducation = new Category("Education", "Educative books");

        ArrayList<Category> programming = new ArrayList<>();
        programming.add(catCreative);
        programming.add(catProgramming);
        programming.add(catEducation);
        ArrayList<Category> education = new ArrayList<>();
        education.add(catEducation);
        ArrayList<Category> creative = new ArrayList<>();
        creative.add(catCreative);
        creative.add(catEducation);

        this.bookRepository = new ArrayList<>();
        bookRepository.add(new Book(counter.incrementAndGet(),
                "Grote Bosatlas, 54e editie.",
                "De Grote Bosatlas",
                " Noordhoff Uitgevers B.V.",
                new ArrayList<String>() {{
                    add("Atlas");
                    add("Landkaarten");
                    add("Informatie");
                    add("Geografie");
                }},
                education));
        bookRepository.add(new Book(counter.incrementAndGet(),
                "Handlettering is hot!",
                "Handlettering doe je zo!",
                "Karin Luttenberg",
                new ArrayList<String>() {{
                    add("Tekenen");
                    add("Schrijven");
                    add("Hobby");
                    add("DoeHetZelf");
                }},
                creative));
        bookRepository.add(new Book(counter.incrementAndGet(),
                "Learn JavaScript and jQuery a nicer way.",
                "JavaScript & JQuery",
                "Jon Duckett",
                new ArrayList<String>() {{
                    add("JavaScript");
                    add("jQuery");
                    add("Browser");
                    add("Programming");
                }},
                programming));
    }

    /**
     * Retourneer alle boeken in de repository.
     *
     * @return
     */
    @ApiOperation(value = "findAllBooks", notes = "Vind alle boeken in het systeem en retourneer deze als een ArrayList.")
    @RequestMapping(value = "/book", method = RequestMethod.GET)
    public ArrayList<Book> findAllBooks() {
        log.debug("findAllBooks called");
        return bookRepository;
    }

    /**
     * Vind een boek op ID.
     * De @Api informatie in de annotaties wordt als documentatie getoond als je in de Swagger UI kijkt.
     *
     * Start de app en ga daarvoor naar http://localhost:8090/swagger-ui.html
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "findById", notes = "Vind een boek op basis van id. Als de id niet aanwezig is volgt een HTTP 404 melding.")
    @RequestMapping(value = "/book/{id}", method = RequestMethod.GET)
    public Book findById(@PathVariable int id) throws Exception{
        log.debug("findById(" + id + ") called");

        if(validateId(id)) {
            log.debug("findById found id");
            Book result = this.bookRepository.get(bookRepository.indexOf(new Book(id)));
            return result;
        } else
            // Deze exception wordt afgehandeld door de RestResponseEntityExceptionHandler
            // Zie ook http://www.baeldung.com/2013/01/31/exception-handling-for-rest-with-spring-3-2/
            // Eigenlijk zou je hier BookNotFoundException willen throwen - moet je zelf maken.
            log.error("findById did not find id");
            throw new Exception("A book with id " + id + " was not found!");
    }

    /**
     * Om fouten te voorkomen moeten we eerst valideren dat het gevraagde id bestaat.
     *
     * @param id
     */
    private boolean validateId(int id) {
        boolean result = this.bookRepository.contains(new Book(id));
        log.debug("validateId " + id + " returned " + result);
        return result;
    }
}
