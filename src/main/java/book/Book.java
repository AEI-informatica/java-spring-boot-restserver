package book;

import category.Category;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Locale;

/**
 * We halen hier een of meer boeken op via de REST API van de boekenserver.
 */
@ApiModel
@JsonIgnoreProperties(ignoreUnknown = true)
public class Book {

    private final int id;
    private final String content;
    private final String title;
    private final String author;
    private ArrayList<String> subjects;
    private ArrayList<Category> categories;

    @Autowired
    public Book(int id, String content, String title, String author,ArrayList<String> subjects, ArrayList<Category> categories) {
        this.id = id;
        this.content = content;
        this.author = author;
        this.title = title;
        this.subjects = subjects;
        this.categories = categories;
    }

    /**
     * Wanneer we boeken in de boekenlijst opzoeken moeten we een
     * boek hebben met het id dat we zoeken. De overige attributen zijn niet van belang
     * omdat ze niet meedoen in de vergelijking van de twee boeken.
     * Zie BookController.validateId()
     *
     * @param id
     */
    @Autowired
    public Book(int id) {
        this.id = id;
        this.content = this.title = this.author = null;
    }

    @ApiModelProperty(position = 1, required = true, value = "uniek nummer dat id van het boek aangeeft")
    public int getId() {
        return id;
    }

    @ApiModelProperty(position = 2, required = false, value = "beschrijving van de inhoud (samenvatting)")
    public String getContent() {
        return content;
    }

    @ApiModelProperty(position = 3, required = false)
    public String getTitle() {
        return title;
    }

    @ApiModelProperty(position = 4, required = false)
    public String getAuthor() {
        return author;
    }

    @ApiModelProperty(position = 5, required = false)
    public ArrayList<String> getSubjects() {
        return subjects;
    }

    @ApiModelProperty(position = 6, required = false)
    public ArrayList<Category> getCategories() {
        return categories;
    }

    /**
     * Om een boek op id terug te kunnen vinden in de ArrayList moeten we een
     * vergelijkingsmethode maken die aangeeft wanneer boeken gelijk zijn aan elkaar.
     * Wij zeggen dat twee boeken gelijk zijn wanneer ze dezelfde id hebben.
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;

        Book book = (Book) o;

        return getId() == book.getId();
    }

    /**
     * Deze methode zorgt dat we een Book in een Hashlist kunnen opslaan. Maken we
     * momenteel nog geen gebruik van.
     *
     * @return
     */
    @Override
    public int hashCode() {
        return (int) (getId() ^ (getId() >>> 32));
    }
}
