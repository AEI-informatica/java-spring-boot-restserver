package category;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * We halen hier een of meer boeken op via de REST API van de boekenserver.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Category {

    private final String name;
    private final String description;

    @Autowired
    public Category(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
