# Spring Boot RESTserver with Swagger  #

Spring RESTserver to provide a REST API to the Spring Library worked example.

### Sources of inspiration ###

* https://spring.io/guides/gs/rest-service/
* http://heidloff.net/article/usage-of-swagger-2-0-in-spring-boot-applications-to-document-apis/

### How do I get set up? ###

* clone or download this repository
* cd /path/to/repo
* mvn clean compile spring-boot:run

Check http://localhost:8090/api/book and http://localhost:8090/api/book/3 in your browser

### Where is Swagger? ###

Check http://localhost:8090/swagger-ui.html in your browser

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact